from flask import Flask
import config    # Configure from .ini files and command line
app = Flask(__name__)

options = config.configuration()

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<page>")
def servepage(page):
    return page
    

@app.errorhandler(404)
def error_404():
    return render_template("404.html")

@app.errorhandler(403)
def error_403():
    return render_template("403.html")


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
