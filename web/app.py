from flask import Flask, render_template
app = Flask(__name__)

@app.errorhandler(403)
def error_403(e):
    return render_template("403.html", name=None), 403

@app.errorhandler(404)
def error_404(e):
    return render_template("404.html", name=None), 404


@app.route("/")
def hello():
    return "UOCIS docker demo!"

config = {"merge_slashes":False}
@app.route("/<path:page>", **config)
def servepage(page):
    print("got page {}".format(page)) 
    try:
        if (".." in page) or ("//" in page) or ("~" in page):
             return error_403(None)
        else:
            return render_template(str(page))
        

    except:
        return error_404(None) 



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
